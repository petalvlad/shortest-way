using System;

namespace ShortestWay.Exceptions
{
    public class LoopLinkException : InvalidPlanException
    {
        public LoopLinkException()
                : base("Loop links are invalid.")
        {

        }
    }
}