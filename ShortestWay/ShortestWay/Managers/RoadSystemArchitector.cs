﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using ShortestWay.Entities;
using ShortestWay.Exceptions;

namespace ShortestWay.Managers
{
    /// <summary>
    /// Represents an architector of <see cref="RoadSystem"/> which reads an xml plan, validate that plan and create an instance of <see cref="RoadSystem"/>
    /// </summary>
    public class RoadSystemArchitector
    {
        private readonly RoadSystemPlanReader _planReader;

        public RoadSystemArchitector()
        {
            _planReader = new RoadSystemPlanReader();
        }

        /// <summary>
        /// Creates new <see cref="RoadSystem"/> from a plan
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> which contains a plan</param>
        /// <returns>The <see cref="RoadSystem"/></returns>
        public RoadSystem BuildRoadSystemFromPlan(Stream stream)
        {

            return BuildRoadSystemFromPlan(_planReader.CreatePlanFromFile(stream));
        }

        /// <summary>
        /// Creates new <see cref="RoadSystem"/> from a plan
        /// </summary>
        /// <param name="xmlPlanPath">The path to xml file which contains a plan</param>
        /// <returns>The <see cref="RoadSystem"/></returns>
        public RoadSystem BuildRoadSystemFromPlan(string xmlPlanPath)
        {
            return BuildRoadSystemFromPlan(_planReader.CreatePlanFromFile(xmlPlanPath));
        }

        private RoadSystem BuildRoadSystemFromPlan(XDocument plan)
        {
            if (plan == null || plan.Root == null || plan.Root.Name != "graph")
            {
                throw new InvalidPlanException();
            }

            if (!plan.Root.HasElements)
            {
                throw new NoNodesException();
            }

            var roadSystem = new RoadSystem();
            foreach (var node in plan.Root.Elements("node"))
            {
                RoadSystemNode roadSystemNode;
                try
                {
                    int id = Convert.ToInt16(node.Attribute("id").Value);
                    var role = RoadSystemNodeRole.Default;
                    var status = RoadSystemNodeStatus.Alive;

                    if (node.Attribute("role") != null)
                    {
                        switch (node.Attribute("role").Value)
                        {
                            case "start":
                                role = RoadSystemNodeRole.Start;
                                break;
                            case "finish":
                                role = RoadSystemNodeRole.Finish;
                                break;
                            default:
                                throw new InvalidNodeRoleException();
                        }
                    }

                    if (node.Attribute("status") != null)
                    {
                        switch (node.Attribute("status").Value)
                        {
                            case "crash":
                                status = RoadSystemNodeStatus.Crash;
                                break;
                            default:
                                throw new InvalidNodeStatusException();
                        }
                    }

                    roadSystemNode = new RoadSystemNode(id, roadSystem, role, status);

                    foreach (var link in node.Elements("link"))
                    {
                        var nodeId = Convert.ToInt16(link.Attribute("ref").Value);
                        var weight = Convert.ToUInt16(link.Attribute("weight").Value);
                        var roadSystemLink = new RoadSystemLink(nodeId, weight, roadSystem);
                        if (roadSystemLink.NodeId == roadSystemNode.Id)
                        {
                            throw new LoopLinkException();
                        }
                        roadSystemNode.AddLink(roadSystemLink);
                    }
                    if (roadSystemNode.LinksCount == 0)
                    {
                        throw new NoLinksException();
                    }
                }

                catch (InvalidPlanException)
                {
                    throw;
                }
                catch (Exception)
                {
                    throw new InvalidPlanException();
                }

                roadSystem.AddNode(roadSystemNode);
            }

            var roadSystemNodes = roadSystem.GetNodes().ToArray();

            if (!roadSystemNodes.Any())
            {
                throw new NoNodesException();
            }


            var startNodes = roadSystemNodes.Where(it => it.Role == RoadSystemNodeRole.Start).ToArray();
            var finishNodes = roadSystemNodes.Where(it => it.Role == RoadSystemNodeRole.Finish).ToArray();

            if (startNodes.Length == 0)
            {
                throw new NoStartException();
            }
            if (startNodes.Length > 1)
            {
                throw new MultipleStartsException();
            }

            if (finishNodes.Length == 0)
            {
                throw new NoFinishException();
            }
            if (finishNodes.Length > 1)
            {
                throw new MultipleFinishesException();
            }

            // Validate each link of each node
            foreach (var roadSystemNode in roadSystemNodes)
            {
                foreach (var roadSystemLink in roadSystemNode.GetLinks())
                {
                    // figuring out that referenced node exists in road system
                    var node = roadSystem.GetNodeById(roadSystemLink.NodeId);
                    if (node == null)
                    {
                        throw new NullNodeException();
                    }

                    // figuring out that referenced node also links back to roadSystemNode
                    var backLink = node.GetLinkByNodeId(roadSystemNode.Id);
                    if (backLink == null)
                    {
                        throw new NullBackLinkException();
                    }

                    // figuring out that the link between nodes has the same weight on the forward and backward ways
                    if (roadSystemLink.Weight != backLink.Weight)
                    {
                        throw new DifferentWeightLinksBetweenSameNodesException();
                    }
                }
            }
            return roadSystem;
        }
    }
}