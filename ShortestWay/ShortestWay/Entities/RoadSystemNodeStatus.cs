﻿namespace ShortestWay.Entities
{
    public enum RoadSystemNodeStatus
    {
        Alive,
        Crash
    }
}