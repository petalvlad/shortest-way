using System;

namespace ShortestWay.Exceptions
{
    public class NoNodesException : InvalidPlanException
    {
        public NoNodesException()
                : base("There are no nodes. Road system should contain at least start and finish nodes.")
        {

        }
    }
}