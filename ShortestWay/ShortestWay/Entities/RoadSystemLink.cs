﻿namespace ShortestWay.Entities
{
    /// <summary>
    /// Represents a link between nodes on the <see cref="RoadSystem"/>
    /// </summary>
    public class RoadSystemLink
    {
        private RoadSystemNode _node;

        /// <summary>
        /// Gets the parent <see cref="RoadSystem"/> of this <see cref="RoadSystemLink"/>
        /// </summary>
        public RoadSystem RoadSystem { get; private set; }

        /// <summary>
        /// Gets the id of <see cref="RoadSystemNode"/> which is referenced by this <see cref="RoadSystemLink"/>
        /// </summary>
        public int NodeId { get; private set; }

        /// <summary>
        /// Gets the <see cref="RoadSystemNode"/> which is referenced by this <see cref="RoadSystemLink"/>
        /// </summary>
        public RoadSystemNode Node { get { return _node ?? (_node = RoadSystem.GetNodeById(NodeId)); } }

        /// <summary>
        /// Gets the weight of this <see cref="RoadSystemLink"/> which basically means the time to reach referenced node if you go through this <see cref="RoadSystemLink"/>
        /// </summary>
        public uint Weight { get; private set; }

        /// <summary>
        /// Returns new <see cref="RoadSystemLink"/>
        /// </summary>
        /// <param name="nodeId">Id of referenced <see cref="RoadSystemNode"/></param>
        /// <param name="weight">Time to reach referenced <see cref="RoadSystemNode"/></param>
        /// <param name="roadSystem">Parent <see cref="RoadSystem"/></param>
        public RoadSystemLink(int nodeId, uint weight, RoadSystem roadSystem)
        {
            RoadSystem = roadSystem;
            NodeId = nodeId;
            Weight = weight;
        }
    }
}