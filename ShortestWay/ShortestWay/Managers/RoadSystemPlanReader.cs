﻿using System.IO;
using System.Xml.Linq;
using ShortestWay.Entities;

namespace ShortestWay.Managers
{
    /// <summary>
    /// Represents a reader of xml plan of <see cref="RoadSystem"/> from different sources
    /// </summary>
    public class RoadSystemPlanReader
    {
        /// <summary>
        /// Returns a <see cref="XDocument"/> constructed from xml file
        /// </summary>
        /// <param name="path">The path to xml file with data of plan</param>
        /// <returns>Returns a <see cref="XDocument"/> constructed from xml file</returns>
        public XDocument CreatePlanFromFile(string path)
        {
            using (var stream = File.OpenRead(path))
            {
                return CreatePlanFromFile(stream);
            }
        }

        /// <summary>
        /// Returns a <see cref="XDocument"/> constructed from xml file
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> which contains the data of plan</param>
        /// <returns>Returns a <see cref="XDocument"/> constructed from xml file</returns>
        public XDocument CreatePlanFromFile(Stream stream)
        {
            return XDocument.Load(stream);
        }
    }
}