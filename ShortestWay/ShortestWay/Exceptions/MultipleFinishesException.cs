using System;

namespace ShortestWay.Exceptions
{
    public class MultipleFinishesException : InvalidPlanException
    {
        public MultipleFinishesException()
                : base("There are multiple finish nodes. Road system should contain only one finish node.")
        {

        }
    }
}