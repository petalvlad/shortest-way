﻿namespace ShortestWay.Entities
{
    public enum RoadSystemNodeRole
    {
        Default,
        Start,
        Finish
    }
}