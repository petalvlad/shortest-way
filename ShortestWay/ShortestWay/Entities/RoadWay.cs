﻿using System.Collections.Generic;
using System.Linq;

namespace ShortestWay.Entities
{
    /// <summary>
    /// Represents a specific way on the <see cref="RoadSystem"/> as an ordered list of <see cref="RoadSystemNode"/>
    /// </summary>
    public class RoadWay : List<RoadSystemNode>
    {
        /// <summary>
        /// Gets an overall time to go through all nodes of this <see cref="RoadWay"/> from start to finish 
        /// </summary>
        public uint Time { get; private set; }

        /// <summary>
        /// Creates new <see cref="RoadWay"/>
        /// </summary>
        /// <param name="time">The time of this <see cref="RoadWay"/></param>
        public RoadWay(uint time)
        {
            Time = time;
        }

        /// <summary>
        /// Returns a string representation of this <see cref="RoadWay"/>
        /// </summary>
        /// <returns>Returns a string representation of this <see cref="RoadWay"/></returns>
        public override string ToString()
        {
            return string.Join("->", this.Select(it => it.Id));
        }
    }
}