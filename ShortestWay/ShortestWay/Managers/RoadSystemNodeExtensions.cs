﻿using ShortestWay.Entities;

namespace ShortestWay.Managers
{
    /// <summary>
    /// Represents extension methods for <see cref="RoadSystemNode"/>
    /// </summary>
    public static class RoadSystemNodeExtensions
    {
        /// <summary>
        /// Create links to <see cref="RoadSystemNode"/> in fluent style
        /// </summary>
        /// <param name="node">The <see cref="RoadSystemNode"/> to be had new <see cref="RoadSystemLink"/></param>
        /// <param name="reference">The <see cref="RoadSystemNode"/> to be referenced by <paramref name="node"/></param>
        /// <param name="weight">The weight of new <see cref="RoadSystemLink"/></param>
        /// <returns>The same <see cref="RoadSystemNode"/> <param name="node"></param></returns>
        public static RoadSystemNode LinkTo(this RoadSystemNode node, RoadSystemNode reference, uint weight)
        {
            node.AddLink(new RoadSystemLink(reference.Id, weight, reference.RoadSystem));
            return node;
        }
    }
}