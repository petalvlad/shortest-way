﻿using ShortestWay.Entities;

namespace ShortestWay.Managers
{
    /// <summary>
    /// Represents an extension of <see cref="RoadSystemNode"/> class with several properties which help to find routes on the <see cref="RoadSystem"/>
    /// </summary>
    public class NavigationNodeWrapper
    {
        /// <summary>
        /// Gets wrapped <see cref="RoadSystemNode"/>
        /// </summary>
        public RoadSystemNode Node { get; private set; }

        /// <summary>
        /// Gets or sets value indicates what the shortest way to this wrapped <see cref="RoadSystemNode"/> was found
        /// </summary>
        public bool IsShortestWayToHereCalculated { get; set; }

        /// <summary>
        /// Gets or sets the minimum needed time to reach this wrapped <see cref="RoadSystemNode"/> from start <see cref="RoadSystemNode"/> on the parent <see cref="RoadSystem"/> of wrapped <see cref="RoadSystemNode"/>
        /// </summary>
        public uint TimeToReach { get; set; }

        /// <summary>
        /// Gets or sets the previous <see cref="RoadSystemNode"/> of this wrapped <see cref="RoadSystemNode"/> on the shortest way from start <see cref="RoadSystemNode"/> on the parent <see cref="RoadSystem"/> of wrapped <see cref="RoadSystemNode"/>
        /// </summary>
        public NavigationNodeWrapper PreviousNodeOnTheShortestWay { get; set; }

        /// <summary>
        /// Creates new <see cref="NavigationNodeWrapper"/>
        /// </summary>
        /// <param name="node">The <see cref="RoadSystemNode"/> to be wrapped</param>
        public NavigationNodeWrapper(RoadSystemNode node)
        {
            Node = node;
            TimeToReach = int.MaxValue;
            PreviousNodeOnTheShortestWay = null;
            IsShortestWayToHereCalculated = false;
        }
    }
}