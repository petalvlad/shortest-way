using System;

namespace ShortestWay.Exceptions
{
    public class NoLinksException : InvalidPlanException
    {
        public NoLinksException()
                : base("Node have no links.")
        {

        }
    }
}