namespace ShortestWay.Exceptions
{
    public class NullBackLinkException : InvalidPlanException
    {
        public NullBackLinkException()
                : base("Node is referenced but has no backward link.")
        {

        }
    }
}