﻿using System;
using ShortestWay.Managers;

namespace ShortestWay
{
    class Program
    {
        static void Main(string[] args)
        {
            string xmlPlanPath;
            if (args.Length > 0)
            {
                xmlPlanPath = args[0];
            }
            else
            {
                Console.WriteLine("Enter the path to xml plan file:");
                xmlPlanPath = Console.ReadLine();
            }

            try
            {
                var roadSystemArchitector = new RoadSystemArchitector();
                var roadSystem = roadSystemArchitector.BuildRoadSystemFromPlan(xmlPlanPath);
                var navigator = new Navigator();
                var shortestWay = navigator.GetShortestWaysOfRoadSystem(roadSystem);
                Console.WriteLine("Shortest way will take {0} time: {1}", shortestWay.Time, shortestWay);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
