﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShortestWay.Entities;
using ShortestWay.Exceptions;
using ShortestWay.Managers;
using ShortestWay.UnitTests.Helpers;

namespace ShortestWay.UnitTests
{
    [TestClass]
    public class RoadSystemArchitectorTests
    {
        private readonly RoadSystemArchitector _architector;

        public RoadSystemArchitectorTests()
        {
            _architector = new RoadSystemArchitector();    
        }

        private RoadSystem BuildRoadSystemFromString(string xmlString)
        {
            using (var stream = xmlString.ToStream())
            {
                return _architector.BuildRoadSystemFromPlan(stream);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidPlanException), "BuildRoadSystemFromPlan should cause InvalidPlanException for plan with not expected root node")]
        public void BuildRoadSystemFromPlan_Should_Cause_InvalidPlanException_For_Plan_With_Not_Expected_Root_Node()
        {
            BuildRoadSystemFromString("<notexpected></notexpected>");
        }

        [TestMethod]
        [ExpectedException(typeof(NoNodesException), "BuildRoadSystemFromPlan should cause NoNodesException for plan without nodes")]
        public void BuildRoadSystemFromPlan_Should_Cause_NoNodesException_For_Plan_Without_Nodes()
        {
            BuildRoadSystemFromString("<graph></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidPlanException), "BuildRoadSystemFromPlan should cause InvalidPlanException for plan with node without id")]
        public void BuildRoadSystemFromPlan_Should_Cause_InvalidPlanException_For_Plan_With_Node_Without_Id()
        {
            BuildRoadSystemFromString("<graph><node></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNodeRoleException), "BuildRoadSystemFromPlan should cause InvalidNodeRoleException for plan with node with invalid role attribute")]
        public void BuildRoadSystemFromPlan_Should_Cause_InvalidNodeRoleException_For_Plan_With_Node_With_Invalid_Role_Attribute()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\" role=\"not_expected_value\"></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNodeStatusException), "BuildRoadSystemFromPlan should cause InvalidNodeStatusException for plan with node with invalid status attribute")]
        public void BuildRoadSystemFromPlan_Should_Cause_InvalidNodeStatusException_For_Plan_With_Node_With_Invalid_Status_Attribute()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\" status=\"not_expected_value\"></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidPlanException), "BuildRoadSystemFromPlan should cause InvalidPlanException for plan with node with link without ref")]
        public void BuildRoadSystemFromPlan_Should_Cause_InvalidPlanException_For_Plan_With_Node_With_Link_Without_Ref()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\"><link weight=\"5\" /></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidPlanException), "BuildRoadSystemFromPlan should cause InvalidPlanException for plan with node with link without weight")]
        public void BuildRoadSystemFromPlan_Should_Cause_InvalidPlanException_For_Plan_With_Node_With_Link_Without_Weight()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\"><link ref=\"2\" /></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(LoopLinkException), "BuildRoadSystemFromPlan should cause LoopLinkException for plan with node with loop link")]
        public void BuildRoadSystemFromPlan_Should_Cause_LoopLinkException_For_Plan_With_Node_With_Loop_Link()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\"><link ref=\"1\" weight=\"5\"/></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(NoLinksException), "BuildRoadSystemFromPlan should cause NoLinksException for plan with node without links")]
        public void BuildRoadSystemFromPlan_Should_Cause_NoLinksException_For_Plan_With_Node_Without_Links()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\"></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(NoNodesException), "BuildRoadSystemFromPlan should cause NoNodesException for plan without expected nodes")]
        public void BuildRoadSystemFromPlan_Should_Cause_NoNodesException_For_Plan_Without_Expected_Nodes()
        {
            BuildRoadSystemFromString("<graph><notexpected></notexpected></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(NoStartException), "BuildRoadSystemFromPlan should cause NoStartException for plan without start node")]
        public void BuildRoadSystemFromPlan_Should_Cause_NoStartException_For_Plan_Without_Start_Node()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\"><link ref=\"2\" weight=\"5\"/></node><node id=\"2\" role=\"finish\"><link ref=\"1\" weight=\"5\"/></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(MultipleStartsException), "BuildRoadSystemFromPlan should cause MultipleStartsException for plan with multiple start nodes")]
        public void BuildRoadSystemFromPlan_Should_Cause_MultipleStartsException_For_Plan_With_Multiple_Start_Nodes()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\" role=\"start\"><link ref=\"2\" weight=\"5\"/></node><node id=\"2\" role=\"start\"><link ref=\"1\" weight=\"5\"/><link ref=\"3\" weight=\"5\"/></node><node id=\"3\" role=\"finish\"><link ref=\"2\" weight=\"5\"/></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(NoFinishException), "BuildRoadSystemFromPlan should cause NoFinishException for plan without finish node")]
        public void BuildRoadSystemFromPlan_Should_Cause_NoFinishException_For_Plan_Without_Finish_Node()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\" role=\"start\"><link ref=\"2\" weight=\"5\"/></node><node id=\"2\" ><link ref=\"1\" weight=\"5\"/></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(MultipleFinishesException), "BuildRoadSystemFromPlan should cause MultipleFinishesException for plan with multiple finish nodes")]
        public void BuildRoadSystemFromPlan_Should_Cause_MultipleFinishesException_For_Plan_With_Multiple_Finish_Nodes()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\" role=\"start\"><link ref=\"2\" weight=\"5\"/></node><node id=\"2\" role=\"finish\"><link ref=\"1\" weight=\"5\"/><link ref=\"3\" weight=\"5\"/></node><node id=\"3\" role=\"finish\"><link ref=\"2\" weight=\"5\"/></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(NullNodeException), "BuildRoadSystemFromPlan should cause NullNodeException for plan with node with link to not existed node")]
        public void BuildRoadSystemFromPlan_Should_Cause_NullNodeException_For_Plan_With_Node_With_Link_To_Not_Existed_Node()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\" role=\"start\"><link ref=\"10\" weight=\"5\"/></node><node id=\"2\" role=\"finish\"><link ref=\"1\" weight=\"5\"/></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(NullBackLinkException), "BuildRoadSystemFromPlan should cause NullBackLinkException for plan with node without backward link")]
        public void BuildRoadSystemFromPlan_Should_Cause_NullBackLinkException_For_Plan_With_Node_Without_Backward_Link()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\" role=\"start\"><link ref=\"2\" weight=\"5\"/></node><node id=\"2\" ><link ref=\"3\" weight=\"5\"/></node><node id=\"3\" role=\"finish\"><link ref=\"2\" weight=\"5\"/></node></graph>");
        }

        [TestMethod]
        [ExpectedException(typeof(DifferentWeightLinksBetweenSameNodesException), "BuildRoadSystemFromPlan should cause DifferentWeightLinksBetweenSameNodesException for plan with link with different weight for forward and backward ways")]
        public void BuildRoadSystemFromPlan_Should_Cause_DifferentWeightLinksBetweenSameNodesException_For_Plan_With_Link_With_Different_Weight_For_Forward_And_Backward_Ways()
        {
            BuildRoadSystemFromString("<graph><node id=\"1\" role=\"start\"><link ref=\"2\" weight=\"5\"/></node><node id=\"2\" role=\"finish\"><link ref=\"1\" weight=\"10\"/></node></graph>");
        }

        [TestMethod]
        public void BuildRoadSystemFromPlan_Should_Return_Not_Null_RoadSystem_For_Valid_Plan()
        {
            var roadSystem = BuildRoadSystemFromString("<graph><node id=\"1\" role=\"start\"><link ref=\"2\" weight=\"5\"/></node><node id=\"2\" role=\"finish\"><link ref=\"1\" weight=\"5\"/></node></graph>");
            Assert.IsNotNull(roadSystem, "BuildRoadSystemFromPlan should return not null RoadSystem for valid plan");
        }
    }
}
