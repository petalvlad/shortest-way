﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShortestWay.Entities
{
    /// <summary>
    /// Represents a model of road system with nodes and links between them
    /// </summary>
    public class RoadSystem
    {
        private readonly List<RoadSystemNode> _nodes;
        private readonly Dictionary<int, RoadSystemNode> _nodesDictionary;
        private RoadSystemNode _startNode;
        private RoadSystemNode _finishNode;

        /// <summary>
        /// Gets a node with role 'start'
        /// </summary>
        public RoadSystemNode StartNode
        {
            get { return _startNode ?? (_startNode = _nodes.Single(it => it.Role == RoadSystemNodeRole.Start)); }
        }

        /// <summary>
        /// Gets a node with role 'finish' 
        /// </summary>
        public RoadSystemNode FinishNode
        {
            get { return _finishNode ?? (_finishNode = _nodes.Single(it => it.Role == RoadSystemNodeRole.Finish)); }
        }

        public RoadSystem()
        {
            _nodes = new List<RoadSystemNode>();
            _nodesDictionary = new Dictionary<int, RoadSystemNode>();
        }

        /// <summary>
        /// Adds a node to the <see cref="RoadSystem"/>
        /// </summary>
        /// <param name="node">The node to be added to the <see cref="RoadSystem"/>. Null value will cause <see cref="ArgumentNullException"/></param>
        public void AddNode(RoadSystemNode node)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }
            _nodes.Add(node);
            _nodesDictionary.Add(node.Id, node);
        }

        /// <summary>
        /// Finds the node by id
        /// </summary>
        /// <param name="nodeId">The id of desired node</param>
        /// <returns>Returns a node with passed id. Returns null if a node was not found</returns>
        public RoadSystemNode GetNodeById(int nodeId)
        {
            RoadSystemNode node;
            _nodesDictionary.TryGetValue(nodeId, out node);
            return node;
        }

        /// <summary>
        /// Returns a read-only collection of nodes
        /// </summary>
        /// <returns>Returns a read-only collection of nodes</returns>
        public IEnumerable<RoadSystemNode> GetNodes()
        {
            return _nodes.AsReadOnly();
        }
    }
}