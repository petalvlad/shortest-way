namespace ShortestWay.Exceptions
{
    public class InvalidNodeStatusException : InvalidPlanException
    {
        public InvalidNodeStatusException()
                : base("Invalid value of node's status attribute.")
        {

        }
    }
}