﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShortestWay.Entities;
using ShortestWay.Exceptions;
using ShortestWay.Managers;

namespace ShortestWay.UnitTests
{
    [TestClass]
    public class NavigatorTests
    {
        private readonly Navigator _navigator;

        public NavigatorTests()
        {
            _navigator = new Navigator();    
        }

        private RoadSystem GetRoadSystemWithWayBetweenStartAndFinish()
        {
            var roadSystem = new RoadSystem();
            var nodes = new[]
                {
                        new RoadSystemNode(1, roadSystem, RoadSystemNodeRole.Start),
                        new RoadSystemNode(2, roadSystem),
                        new RoadSystemNode(3, roadSystem),
                        new RoadSystemNode(4, roadSystem),
                        new RoadSystemNode(5, roadSystem),
                        new RoadSystemNode(6, roadSystem, RoadSystemNodeRole.Finish)
                };

            nodes[0].LinkTo(nodes[1], 7).LinkTo(nodes[2], 9).LinkTo(nodes[5], 14);
            nodes[1].LinkTo(nodes[0], 7).LinkTo(nodes[2], 10).LinkTo(nodes[3], 15);
            nodes[2].LinkTo(nodes[0], 9).LinkTo(nodes[1], 10).LinkTo(nodes[3], 11).LinkTo(nodes[5], 2);
            nodes[3].LinkTo(nodes[1], 15).LinkTo(nodes[2], 11).LinkTo(nodes[4], 6);
            nodes[4].LinkTo(nodes[3], 6).LinkTo(nodes[5], 9);
            nodes[5].LinkTo(nodes[0], 14).LinkTo(nodes[2], 2).LinkTo(nodes[4], 9);

            foreach (var node in nodes)
            {
                roadSystem.AddNode(node);
            }

            return roadSystem;
        }

        private RoadSystem GetRoadSystemWithWayBetweenStartAndFinishButCrashesOnTheWay()
        {
            var roadSystem = new RoadSystem();
            var nodes = new[]
                {
                        new RoadSystemNode(1, roadSystem, RoadSystemNodeRole.Start),
                        new RoadSystemNode(2, roadSystem),
                        new RoadSystemNode(3, roadSystem),
                        new RoadSystemNode(4, roadSystem, RoadSystemNodeRole.Default, RoadSystemNodeStatus.Crash),
                        new RoadSystemNode(5, roadSystem, RoadSystemNodeRole.Finish),
                        new RoadSystemNode(6, roadSystem, RoadSystemNodeRole.Default, RoadSystemNodeStatus.Crash)
                };

            nodes[0].LinkTo(nodes[1], 7).LinkTo(nodes[2], 9).LinkTo(nodes[5], 14);
            nodes[1].LinkTo(nodes[0], 7).LinkTo(nodes[2], 10).LinkTo(nodes[3], 15);
            nodes[2].LinkTo(nodes[0], 9).LinkTo(nodes[1], 10).LinkTo(nodes[3], 11).LinkTo(nodes[5], 2);
            nodes[3].LinkTo(nodes[1], 15).LinkTo(nodes[2], 11).LinkTo(nodes[4], 6);
            nodes[4].LinkTo(nodes[3], 6).LinkTo(nodes[5], 9);
            nodes[5].LinkTo(nodes[0], 14).LinkTo(nodes[2], 2).LinkTo(nodes[4], 9);

            foreach (var node in nodes)
            {
                roadSystem.AddNode(node);
            }

            return roadSystem;
        }

        private RoadSystem GetRoadSystemWithoutWayBetweenStartAndFinish()
        {
            var roadSystem = new RoadSystem();
            var nodes = new[]
                {
                        new RoadSystemNode(1, roadSystem, RoadSystemNodeRole.Start),
                        new RoadSystemNode(2, roadSystem),
                        new RoadSystemNode(3, roadSystem),
                        new RoadSystemNode(4, roadSystem),
                        new RoadSystemNode(5, roadSystem),
                        new RoadSystemNode(6, roadSystem, RoadSystemNodeRole.Finish)
                };

            nodes[0].LinkTo(nodes[1], 7);
            nodes[1].LinkTo(nodes[0], 7);
            nodes[2].LinkTo(nodes[3], 11).LinkTo(nodes[5], 2);
            nodes[3].LinkTo(nodes[2], 11).LinkTo(nodes[4], 6);
            nodes[4].LinkTo(nodes[3], 6).LinkTo(nodes[5], 9);
            nodes[5].LinkTo(nodes[2], 2).LinkTo(nodes[4], 9);

            foreach (var node in nodes)
            {
                roadSystem.AddNode(node);
            }

            return roadSystem;
        }

        [TestMethod]
        public void GetShortestWaysOfRoadSystem_Should_Return_Shortest_Way_If_That_Way_Exists()
        {
            var shortestWay = _navigator.GetShortestWaysOfRoadSystem(GetRoadSystemWithWayBetweenStartAndFinish());
            Assert.IsNotNull(shortestWay, "GetShortestWaysOfRoadSystem should return shortest way if that way exists in road system");
        }

        [TestMethod]
        [ExpectedException(typeof(NoWayException), "GetShortestWaysOfRoadSystem should cause NoWayException if there is no way between start and finish")]
        public void GetShortestWaysOfRoadSystem_Should_Cause_NoWayException_If_There_Is_No_Way_Between_Start_And_Finish()
        {
            _navigator.GetShortestWaysOfRoadSystem(GetRoadSystemWithoutWayBetweenStartAndFinish());
        }

        [TestMethod]
        [ExpectedException(typeof(NoWayException), "GetShortestWaysOfRoadSystem should cause NoWayException if there are crashes on the way between start and finish")]
        public void GetShortestWaysOfRoadSystem_Should_Cause_NoWayException_If_There_Are_Crashes_On_The_Way_Between_Start_And_Finish()
        {
            _navigator.GetShortestWaysOfRoadSystem(GetRoadSystemWithWayBetweenStartAndFinishButCrashesOnTheWay());
        }
    }
}
