namespace ShortestWay.Exceptions
{
    public class NullNodeException : InvalidPlanException
    {
        public NullNodeException()
                : base("Node does not exist.")
        {

        }
    }
}