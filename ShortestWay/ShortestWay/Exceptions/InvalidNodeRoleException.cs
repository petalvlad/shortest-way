namespace ShortestWay.Exceptions
{
    public class InvalidNodeRoleException : InvalidPlanException
    {
        public InvalidNodeRoleException()
                : base("Invalid value of node's role attribute.")
        {

        }
    }
}