using System;

namespace ShortestWay.Exceptions
{
    public class MultipleStartsException : InvalidPlanException
    {
        public MultipleStartsException()
                : base("There are multiple start nodes. Road system should contain only one start node.")
        {

        }
    }
}