﻿using System;

namespace ShortestWay.Exceptions
{
    public class InvalidPlanException : Exception
    {
        private const string ErrorMessage = "Invalid xml structure.";

        public InvalidPlanException()
            : this(ErrorMessage)
        {
            
        }

        public InvalidPlanException(string message) : base(message)
        {
            
        }

        public InvalidPlanException(InvalidPlanException ex)
            : base(string.Format("{0} {1}", ErrorMessage, ex.Message))
        {

        }
    }
}