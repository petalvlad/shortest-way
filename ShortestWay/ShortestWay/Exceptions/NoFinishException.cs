using System;

namespace ShortestWay.Exceptions
{
    public class NoFinishException : InvalidPlanException
    {
        public NoFinishException()
                : base("There is no finish node.")
        {

        }
    }
}