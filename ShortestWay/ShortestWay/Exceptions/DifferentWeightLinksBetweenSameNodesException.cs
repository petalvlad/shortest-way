namespace ShortestWay.Exceptions
{
    public class DifferentWeightLinksBetweenSameNodesException : InvalidPlanException
    {
        public DifferentWeightLinksBetweenSameNodesException()
                : base("Link has different weight for forward and backward ways.")
        {

        }
    }
}