﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShortestWay.Managers;
using ShortestWay.UnitTests.Helpers;

namespace ShortestWay.UnitTests
{
    [TestClass]
    public class RoadSystemPlanReaderTests
    {
        private readonly RoadSystemPlanReader _reader;

        public RoadSystemPlanReaderTests()
        {
            _reader = new RoadSystemPlanReader();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CreatePlanFromFile_With_Empty_Path_Should_Cause_ArgumentException()
        {
            _reader.CreatePlanFromFile(string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void CreatePlanFromFile_With_Invalid_Path_Should_Cause_Exception()
        {
            _reader.CreatePlanFromFile("invalid path");
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void CreatePlanFromFile_With_NotExist_File_Path_Should_Cause_Exception()
        {
            _reader.CreatePlanFromFile("C:\\not.exist");
        }

        [TestMethod]
        [ExpectedException(typeof(XmlException))]
        public void CreatePlanFromFile_With_Empty_Xml_Should_Cause_XmlException()
        {
            using (var stream = string.Empty.ToStream())
            {
                _reader.CreatePlanFromFile(stream);
            }
        }

        [TestMethod]
        public void CreatePlanFromFile_With_Valid_Xml_Should_Return_Not_Null_XDocument()
        {
            const string xml = "<graph><node id=\"1\"><link ref=\"2\" weight=\"5\" /> </node> <node id=\"2\"> <link ref=\"1\" weight=\"5\" /> </node> </graph>";
            using (var stream = xml.ToStream())
            {
                var plan = _reader.CreatePlanFromFile(stream);
                Assert.IsNotNull(plan, "Plan should not be null");
            }
        }
    }
}
