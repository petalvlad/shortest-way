﻿using System;
using System.Collections.Generic;

namespace ShortestWay.Entities
{
    /// <summary>
    /// Represents a node on the <see cref="RoadSystem"/>
    /// </summary>
    public class RoadSystemNode
    {
        private readonly List<RoadSystemLink> _links;
        private readonly Dictionary<int, RoadSystemLink> _linkedNodes;

        /// <summary>
        /// Gets the id of this <see cref="RoadSystemNode"/>
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Gets the role of this <see cref="RoadSystemNode"/>
        /// </summary>
        public RoadSystemNodeRole Role { get; private set; }

        /// <summary>
        /// Gets the status of this <see cref="RoadSystemNode"/>
        /// </summary>
        public RoadSystemNodeStatus Status { get; private set; }

        /// <summary>
        /// Gets the parent <see cref="RoadSystem"/> of this <see cref="RoadSystemNode"/>
        /// </summary>
        public RoadSystem RoadSystem { get; private set; }

        /// <summary>
        /// Gets the count of <see cref="RoadSystemLink"/> of this <see cref="RoadSystemNode"/>
        /// </summary>
        public int LinksCount { get { return _links.Count; } }

        /// <summary>
        /// Returns new <see cref="RoadSystemNode"/>
        /// </summary>
        /// <param name="id">The id of new <see cref="RoadSystemNode"/></param>
        /// <param name="roadSystem">The parent <see cref="RoadSystem"/> of new <see cref="RoadSystemNode"/></param>
        /// <param name="role">The role of new <see cref="RoadSystemNode"/></param>
        /// <param name="status">The status of new <see cref="RoadSystemNode"/></param>
        public RoadSystemNode(int id, RoadSystem roadSystem, RoadSystemNodeRole role = RoadSystemNodeRole.Default, RoadSystemNodeStatus status = RoadSystemNodeStatus.Alive)
        {
            RoadSystem = roadSystem;
            Id = id;
            Role = role;
            Status = status;
            _links = new List<RoadSystemLink>();
            _linkedNodes = new Dictionary<int, RoadSystemLink>();
        }

        /// <summary>
        /// Adds a <see cref="RoadSystemLink"/> to links of this <see cref="RoadSystemNode"/>
        /// </summary>
        /// <param name="link">The <see cref="RoadSystemLink"/> to be added to links of <see cref="RoadSystemNode"/></param>
        public void AddLink(RoadSystemLink link)
        {
            if (link == null)
            {
                throw new ArgumentNullException("link");
            }
            _links.Add(link);
            _linkedNodes.Add(link.NodeId, link);
        }

        /// <summary>
        /// Gets a read-only collection of <see cref="RoadSystemLink"/> of this <see cref="RoadSystemNode"/>
        /// </summary>
        /// <returns>The read-only collection of <see cref="RoadSystemLink"/></returns>
        public IEnumerable<RoadSystemLink> GetLinks()
        {
            return _links.AsReadOnly();
        }

        /// <summary>
        /// Gets a <see cref="RoadSystemLink"/> by id of <see cref="RoadSystemNode"/> which is referenced by that <see cref="RoadSystemLink"/>
        /// </summary>
        /// <param name="nodeId">The id of <see cref="RoadSystemNode"/></param>
        /// <returns>The <see cref="RoadSystemLink"/> which link to <see cref="RoadSystemNode"/> with <paramref name="nodeId"/>. Returns null if a link was not found</returns>
        public RoadSystemLink GetLinkByNodeId(int nodeId)
        {
            RoadSystemLink link;
            _linkedNodes.TryGetValue(nodeId, out link);
            return link;
        }
    }
}