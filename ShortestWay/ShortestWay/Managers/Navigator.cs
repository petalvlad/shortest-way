﻿using System.Collections.Generic;
using System.Linq;
using ShortestWay.Entities;
using ShortestWay.Exceptions;

namespace ShortestWay.Managers
{
    /// <summary>
    /// Represents a class which calculates shortest way for given <see cref="RoadSystem"/>
    /// </summary>
    public class Navigator
    {
        /// <summary>
        /// Returns the shortest way of given <see cref="RoadSystem"/> if it exists and causes <see cref="NoWayException"/> otherwise
        /// </summary>
        /// <param name="roadSystem">The <see cref="RoadSystem"/> to be discovered for the shortest way</param>
        /// <returns></returns>
         public RoadWay GetShortestWaysOfRoadSystem(RoadSystem roadSystem)
         {
             var navigationNodes = roadSystem.GetNodes().ToDictionary(it => it.Id, it => new NavigationNodeWrapper(it));
             var startNavigationNode = navigationNodes[roadSystem.StartNode.Id];

             // basically the Dijkstra algorithm
             startNavigationNode.TimeToReach = 0;
             var currentNode = startNavigationNode;
             var haveNodesToGoTo = true;

             while (haveNodesToGoTo)
             {
                 currentNode.IsShortestWayToHereCalculated = true;
                 var reachebleNodes = currentNode.Node.GetLinks().Select(it => navigationNodes[it.NodeId]).Where(it => !it.IsShortestWayToHereCalculated).ToArray();

                 foreach (var reachebleNode in reachebleNodes)
                 {
                     var timeToReach = currentNode.TimeToReach +
                                     currentNode.Node.GetLinkByNodeId(reachebleNode.Node.Id).Weight;
                     if (timeToReach < reachebleNode.TimeToReach)
                     {
                         reachebleNode.TimeToReach = timeToReach;
                         reachebleNode.PreviousNodeOnTheShortestWay = currentNode;
                     }
                 }

                 var visitedNavigationNodesWithoutShortestWay =
                         navigationNodes.Values.Where(it =>
                                                      !it.IsShortestWayToHereCalculated &&
                                                      it.Node.Status != RoadSystemNodeStatus.Crash &&
                                                      it.TimeToReach != int.MaxValue).ToArray();

                 if (visitedNavigationNodesWithoutShortestWay.Length > 0)
                 {
                     var minTimeToReach = visitedNavigationNodesWithoutShortestWay.Min(it => it.TimeToReach);
                     currentNode = visitedNavigationNodesWithoutShortestWay.First(it => it.TimeToReach == minTimeToReach);
                 }
                 else
                 {
                     haveNodesToGoTo = false;
                 }
             }

             var finishNavigationNode = navigationNodes[roadSystem.FinishNode.Id];
             if (!finishNavigationNode.IsShortestWayToHereCalculated)
             {
                 throw new NoWayException();
             }

             // Create the roadway with time to reach the finish node
             var roadWay = new RoadWay(finishNavigationNode.TimeToReach);
             // And create list of nodes included in that way by going from finish to start node
             var reverseWay = new List<RoadSystemNode>(new[] { finishNavigationNode.Node });
             var navigationNode = finishNavigationNode;
             while (navigationNode.Node != startNavigationNode.Node)
             {
                 navigationNode = navigationNode.PreviousNodeOnTheShortestWay;
                 reverseWay.Add(navigationNode.Node);
             }
             // Finally reverse the list of nodes to rigth forward order
             reverseWay.Reverse();
             roadWay.AddRange(reverseWay);
             return roadWay;
         }
    }
}