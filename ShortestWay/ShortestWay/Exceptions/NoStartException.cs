using System;

namespace ShortestWay.Exceptions
{
    public class NoStartException : InvalidPlanException
    {
        public NoStartException()
                : base("There is no start node.")
        {

        }
    }
}