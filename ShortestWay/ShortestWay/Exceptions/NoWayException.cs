using System;

namespace ShortestWay.Exceptions
{
    public class NoWayException : InvalidPlanException
    {
        public NoWayException()
                : base("There is no way from start to finish.")
        {

        }
    }
}